<?php
namespace app;
use app\Post;
use \PDO;
class Post {
   
    private $pdoConnection;
    public $postzap;
    //public $data1;
        /**
     * Подключение к БД
     */
    public function __construct() {
        $this->pdoConnection = new PDO('mysql:dbname=forum;charset=utf8;host=localhost;port=3307', 'root', '',[PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
        }
    /**
     * Выводим посты конкретного пользователя
     */    
    public function execute($id_user) {
        //$postzap = $this->pdoConnection->prepare("select event_date, message, login from posts where id_author = :id_author");
        $postzap = $this->pdoConnection->prepare("select u.login, p.event_date, p.message from users as u, posts as p where u.id_user = p.id_author and p.id_author = :id_author");
        $postzap->execute(array(':id_author' => $id_user));
        $data1 = $postzap->fetchAll();
        //print_r($data1);
        return $data1;
        //$_SESSION['data'] = $data;
    }
     /**
     * Выводим все посты
     */   
    public function execute1() {
        $postzap = $this->pdoConnection->query("select u.login, p.event_date, p.message from users as u, posts as p where u.id_user = p.id_author order by p.event_date desc");
        $data1 = $postzap->fetchAll();
        //print_r($data1);
        return $data1;
}
}

