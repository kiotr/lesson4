<?php
namespace app;
use app\UserModel;
use PDO;

    Class UserModel {
     private $pdoConnection;
        /**
     * Подключение к БД
     */
    public function __construct() {
        $this->pdoConnection = new PDO('mysql:dbname=forum;charset=utf8;host=localhost;port=3307', 'root', '',[PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
        }
    
    /**
     * Отправка запроса к БД
     */
    public function getuser($login, $password) {
        
        $prezap = $this->pdoConnection->prepare('select id_user, login from users where login = :login and password = :password');
        $prezap->execute(array(':login' => $login, ':password' => $password));
        $data = $prezap->fetch(PDO::FETCH_ASSOC);
        //$_SESSION['user'] = $data;
        return $data; 
                   
    }
    }