<?php
namespace app;

require_once __DIR__ . '\header.php';
use app\UserModel;
use app\Session;

Class UserLogin {    
              //public function one(){
                //  echo 'ok';
              //}    */   
    public function execute($params) {
        $login = $params['login'];
        $password = $params['password'];
        $UserModel = new UserModel();
        $data = $UserModel->getuser($login, $password);
        Session::setSession($data);
        header('location: GetPost.php');
    }
}